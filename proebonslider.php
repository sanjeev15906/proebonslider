<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  System.proebonslide
 * @author      Sanjeev kumar<sanjeev1.raghuwanshi@gmail.com>
 * @copyright  2019 sanjeev kumar
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC');
jimport('joomla.plugin.plugin');

$lang =  JFactory::getLanguage();
$lang->load('plg_system_proebonslider', JPATH_ADMINISTRATOR);

class PlgSystemProebonslider extends JPlugin
{






public function onAfterRender()
{

       $mainframe = JFactory::getApplication();
        // No HTML content, no need for forms
        if (JFactory::getDocument()->getType() != 'html')
        {
            return false;
        }

        // Backend doesn't need forms being loaded
        if ($mainframe->isAdmin())
        {
            return false;
        }

        // Are we editing an article?
        $option = $mainframe->input->get('option');
        $task   = $mainframe->input->get('task');
        if ($option == 'com_content' && $task == 'edit')
        {
            return false;
        }

        $html = JFactory::getApplication()->getBody();

        if (strpos($html, '</head>') !== false)
        {
            list($head, $content) = explode('</head>', $html, 2);
        }
        else
        {
            $content = $html;
        }


        // Something is wrong here
        if (empty($content))
        {
            return false;
        }

        // No placeholder, don't run
        if (strpos($content, '{proebonslide ') === false)
        {
            return false;
        }

        // expression to search for
        $pattern = '#\{proebonslide ([0-9]+)\}#i';
        if(preg_match_all($pattern, $content, $matches))
        {
            foreach ($matches[0] as $j => $match)
            {

             $eventId = $matches[1][$j];
             $data    =  $this::displayEvent2($eventId);
             $content =  preg_replace($pattern, $data, $content,1);
            }

        }


 $pattern2 = '#\{proebnext\}#';
 if(preg_match_all($pattern2, $content, $matches))
  {
      foreach ($matches[0] as $j => $match)
      {
             $data    =  $this::displayEvent2("featured");
             $content =  preg_replace($pattern2, $data, $content,1);
      }
 
    }


            $html = isset($head) ? ($head . '</head>' . $content) : $content;
           JFactory::getApplication()->setBody($html);  
        

  }




	public function onContentPrepare($context, &$article, &$params, $limitstart = 0)
	{		
		$app = JFactory::getApplication();
		
		if ($app->getName() != 'site')
		{
			return true;
		}
		
		if (strpos($article->text, 'proebonslide') === false)
		{
			return true;
		}
		
		$regex         = "#{proebonslide (\d+)}#s";
		$article->text = preg_replace_callback($regex, array(&$this, 'displayEvent'), $article->text);

		return true;
	}

	
public function displayEvent2($event_id)
  {
    $htmlData = "";

if($event_id){
     if($event_id != "featured")
     {
         $row = $this::getEvents($event_id);
     }else{
            $row = $this::getEventNext("featured");
     }




if($row){
  $htmlData="";
  $location = $this::getLocation($row->location_id);
      //$location = explode(",", $location);
    $htmlData .= '<h3 class="sppb-addon-title">'.$row->title.'</h3>
    <div class="sppb-addon-content date-location">
    <p class="event_date">'.date("l, F j, Y H:i A", strtotime($row->event_date)).'</p> 
    <p class="location_address">'.$location.'</p></div>
    <p class="proebonslide_button_box"> 
      <a href="index.php?option=com_eventbooking&view=event&id='.$row->id.'" class="btn btn-primery">
      '.JText::_("PLG_SYSTEM_PROEBONSLIDER_JOIN_US_BUTTON").'</a></p>';

    }else{
      $config   = JFactory::getConfig();
      $sitename =  $config->get('sitename');
        if(trim($supporting_html) != "")
        $htmlData .= $supporting_html;
        else{
          $htmlData .= "<h3 class='sppb-addon-title'>".str_replace("{SITE}", $sitename, JText::_("PLG_SYSTEM_PROEBONSLIDE_WELCOME"))."</h3>"; 
          $htmlData .= "<p class='event_date'>".JText::_('PLG_SYSTEM_PROEBONSLIDE_SITE_MESSAGE')."</p>";  
         }
          $htmlData .= "<br><a class='btn' href='".$button_link."' target='_blank'>".JText::_('PLG_SYSTEM_BUTTON_CLICK_HERE')."</a>";
      }


   }else{
    $htmlData =  JText::_('PLG_SYSTEM_PLEASE_ENTER_EVENT_ID');
   }

       return  $htmlData;

  }



	/**
	 * Display detail information of the given event
	 *
	 * @param $matches
	 *
	 * @return string
	 * @throws Exception
	 */
	public function displayEvent(&$matches)
	{

     if($matches[1])
     {
     	 $row = $this::getEvents($matches[1]);


?>
<div class="mod_proebonslide proebslide<?php //echo $moduleclass_sfx; ?>">
<?php
if($row){

?>
<h3 class="sppb-addon-title"><?php echo $row->title;; ?></h3>   


<div class="sppb-addon-content date-location">
<p class="event_date"><?php echo date("l, F j, Y H:i A", strtotime($row->event_date)); ?></p> 

<p class="location_address">
<?php
echo $location = $this::getLocation($row->location_id);
$location = explode(",", $location);
?>
</p>
</div>
<p class="proebonslide_button_box"> 
  <a href="index.php?option=com_eventbooking&view=event&id=<?php echo $row->id; ?>" class="btn btn-primery"><?php echo JText::_("PLG_SYSTEM_PROEBONSLIDER_JOIN_US_BUTTON"); ?></a>
</p>
<?php 
}else{

$config = JFactory::getConfig();
$sitename =  $config->get( 'sitename' );

  if(trim($supporting_html) != "")
  echo $supporting_html;
  else{
    echo "<h3 class='sppb-addon-title'>".str_replace("{SITE}", $sitename, JText::_("PLG_SYSTEM_PROEBONSLIDE_WELCOME"))."</h3>"; 
    echo "<p class='event_date'>".JText::_('PLG_SYSTEM_PROEBONSLIDE_SITE_MESSAGE')."</p>";  
   }
 echo "<br><a class='btn' href='".$button_link."' target='_blank'>".JText::_('PLG_SYSTEM_BUTTON_CLICK_HERE')."</a>";

} ?>
</div>


<?php
     }else{

     	echo JText::_('PLG_SYSTEM_PLEASE_ENTER_EVENT_ID');
     }

	}



      public static function getEventNext($event_type="featured")
      {
     
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from($db->quoteName('#__eb_events'));
        $query->where($db->quoteName('published') . ' = 1');
       
         if($event_type == "featured")
         {
            $query->where($db->quoteName('featured') . ' = 1');
            $query->where('((YEAR(event_date) = YEAR(NOW()) AND MONTH(event_date) = MONTH(NOW()) AND DAY(event_date) > DAY(NOW())) OR (YEAR(event_date) > YEAR(NOW())) ||  (YEAR(event_date) = YEAR(NOW()) AND MONTH(event_date) > MONTH(NOW()))) ');
         }else{
            $query->where('((YEAR(event_date) = YEAR(NOW()) AND MONTH(event_date) = MONTH(NOW()) AND DAY(event_date) > DAY(NOW())) OR (YEAR(event_date) > YEAR(NOW())) ||  (YEAR(event_date) = YEAR(NOW()) AND MONTH(event_date) > MONTH(NOW()))) ');
         }

        $query->order('event_date ASC');
        $db->setQuery($query);
        $results = $db->loadObject();

        if(@count($results)<1)
        {

        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from($db->quoteName('#__eb_events'));
        $query->where($db->quoteName('published') . ' = 1');
        $query->where('((YEAR(event_date) = YEAR(NOW()) AND MONTH(event_date) = MONTH(NOW()) AND DAY(event_date) > DAY(NOW())) OR (YEAR(event_date) > YEAR(NOW())) ||  (YEAR(event_date) = YEAR(NOW()) AND MONTH(event_date) > MONTH(NOW()))) ');
        $query->order('event_date ASC');
        $db->setQuery($query, 0, 1);
        $results = $db->loadObject();
        
        }

        return $results;
      }

      public static function getEvents($event_id)
      {
     
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from($db->quoteName('#__eb_events'));
        $query->where($db->quoteName('id') . ' = '.$event_id);
        $query->where($db->quoteName('published') . ' = 1');
        $db->setQuery($query, 0, 1);
        $data = $db->loadObject();
        return $data;        
      }



 static function getLocation($loc_id)
  {

          $db = JFactory::getDbo();
          $query = $db->getQuery(true);
          $query
              ->select(array('a.address'))
              ->from($db->quoteName('#__eb_locations', 'a'))
              ->where($db->quoteName('a.id') . ' = '.$loc_id);
          $db->setQuery($query);
          $data = $db->loadObject();
          if($data)
          return $data->address;
  }







}

